from django.urls import path
from sales_rest.views import api_list_salespeople, api_list_customers, api_list_sales, api_sales_by_salesperson

urlpatterns = [
    path('salespeople/', api_list_salespeople, name='list_salespeople'),
    path('customers/', api_list_customers, name='list_customers'),
    path('sales/', api_list_sales, name='list_sales'),  
    path('sales/salesperson/<int:salesperson_id>/', api_sales_by_salesperson, name='api_sales_by_salesperson'),
]