from django.contrib import admin
from .models import AutomobileVO, Customer, SalesPerson, Sale

# Optional: Custom admin interface for the Sale model
class SaleAdmin(admin.ModelAdmin):
    list_display = ('automobile', 'customer', 'salesperson', 'price')  # Fields to display in the list view
    list_filter = ('salesperson', 'customer')  # Filters on the right sidebar
    search_fields = ('automobile__vin', 'customer__first_name', 'customer__last_name', 'salesperson__first_name', 'salesperson__last_name')  # Searchable fields
    raw_id_fields = ('automobile', 'customer', 'salesperson')  # Use raw_id fields for ForeignKey fields to improve performance on large datasets

# Register your models here.
admin.site.register(AutomobileVO)
admin.site.register(Customer)
admin.site.register(SalesPerson)

# Register the Sale model with the custom admin interface
admin.site.register(Sale, SaleAdmin)