# CarCar

Team:

- Kassandra Vasquez - Service
- Christopher Brown - Sales

## How to Run this App

1. Fork this repository

2. Clone the forked repository onto your local computer:
   git clone <<respository.url.here>>

3. Build and run the project using Docker with these commands:
   docker volume create beta-data
   docker-compose build
   docker-compose up

4. Make sure all of the Docker containers are running

5. Go to project in the browser: http://localhost:3000/

## Diagram

CarCar has 3 microservices that interact with one another:

- Inventory
- Services
- Sales

![image](<Project-Beta Diagram.png>)
obsidian://open?vault=Obsidian%20Vault&file=Project-Beta%20Diagram

## API Documentation

## VEHICLE

- Creating a vehicle
  {
  "name": "Sebring",
  "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/771/Chrysler_Sebring_front_20090302.jpg320px-Chrysler_Sebring_front_20090302.jpg",
  "manufacturer_id": 1
  }

- Return value for list and creating of vehicles
  {
  "models": [
  {
  "href": "/api/models/1/",
  "id": 1,
  "name": "Sebring",
  "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
  "manufacturer": {
  "href": "/api/manufacturers/1/",
  "id": 1,
  "name": "Daimler-Chrysler"
  }
  }
  ]
  }

- Return value of getting details of specific vehicle model
  {
  "href": "/api/models/1/",
  "id": 1,
  "name": "Sebring",
  "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
  "manufacturer": {
  "href": "/api/manufacturers/1/",
  "id": 1,
  "name": "Daimler-Chrysler"
  }
  }

## MANUFACTURER

- Creating and updating manufacturer
  {
  "name": "Chrysler"
  }

- Return value for creating, getting, and updating specific manufacturer
  {
  "href": "/api/manufacturers/1/",
  "id": 1,
  "name": "Chrysler"
  }

- Return value for list of manufacturers
  {
  "manufacturers": [
  {
  "href": "/api/manufacturers/1/",
  "id": 1,
  "name": "Daimler-Chrysler"
  }
  ]
  }

## AUTOMOBILE

- Creating automobile
  {
  "color": "red",
  "year": 2012,
  "vin": "1C3CC5FB2AN120174",
  "model_id": 1
  }

- Updating automobile
  {
  "color": "red",
  "year": 2012,
  "sold": true
  }

- Return value for details of a specific automobile with VIN
  {
  "href": "/api/automobiles/1C3CC5FB2AN120174/",
  "id": 1,
  "color": "yellow",
  "year": 2013,
  "vin": "1C3CC5FB2AN120174",
  "model": {
  "href": "/api/models/1/",
  "id": 1,
  "name": "Sebring",
  "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
  "manufacturer": {
  "href": "/api/manufacturers/1/",
  "id": 1,
  "name": "Daimler-Chrysler"
  }
  },
  "sold": false
  }

- Return value for list and creating of automobiles
  {
  "autos": [
  {
  "href": "/api/automobiles/1C3CC5FB2AN120174/",
  "id": 1,
  "color": "yellow",
  "year": 2013,
  "vin": "1C3CC5FB2AN120174",
  "model": {
  "href": "/api/models/1/",
  "id": 1,
  "name": "Sebring",
  "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
  "manufacturer": {
  "href": "/api/manufacturers/1/",
  "id": 1,
  "name": "Daimler-Chrysler"
  }
  },
  "sold": false
  }
  ]
  }

## APPOINTMENT

- Creating appointment
  {
  "date_time": "2024-03-06",
  "reason": "winshield",
  "status": "created",
  "vin": "VINNUMBERNEWW",
  "customer": "First Last",
  "technician": 1
  }

- Return value for list and creating appointment
  {
  "appointments": [
  {
  "reason": "dent",
  "vin": "KNJDKDMOLS",
  "status": "finished",
  "date_time": "2024-03-06T00:00:00+00:00",
  "customer": "customer_name",
  "id": 22,
  "technician": {
  "last_name": "last_name",
  "id": 1,
  "employee_id": "44",
  "first_name": "first_name"
  }
  }
  ]
  }

- Return valuue for details of a specific appointment
  {
  "appointment": {
  "reason": "winshield",
  "vin": "AJNUFNJSMKOM",
  "status": "created",
  "date_time": "2024-03-06T00:00:00+00:00",
  "customer": "customer_name",
  "id": 24,
  "technician": {
  "last_name": "last_name",
  "id": 1,
  "employee_id": "44",
  "first_name": "first_name"
  }
  }
  }

- Return value for delete appointment
  {
  "deleted": true
  }

## TECHNICIAN

- Creating technicians
  {
  "first_name": "first_name",
  "last_name": "last_name",
  "employee_id": 22
  }

- Return value for list and creating a technician
  {
  "technicians": [
  {
  "first_name": "Eric",
  "employee_id": "44",
  "id": 1,
  "last_name": "Johnson"
  }
  ]
  }

- Return value for getting details of a specific technician
  {
  "technician": {
  "first_name": "Sally",
  "employee_id": "25",
  "last_name": "Shores"
  }
  }

- Return value for delete technician
  {
  "deleted": true
  }

### URLs and Ports

List Technicians http://localhost:8080/api/technicians/ (GET)
Create Technician http://localhost:8080/api/technicians/ (POST)
Show Technician http://localhost:8080/api/technicians/id/ (GET)
Delete Technician http://localhost:8080/api/technicians/id/ (DELETE)

List Appointments http://localhost:8080/api/appointments/ (GET)
Create Appointment http://localhost:8080/api/appointments/ (POST)
Show Appointment http://localhost:8080/api/appointments/id/ (GET)
Delete Appointment http://localhost:8080/api/appointments/id/ (DELETE)
Update Appointment Status to 'canceled' http://localhost:8080/api/appointments/:id/cancel/ (PUT)
Update Appointment Status to 'finished' http://localhost:8080/api/appointments/:id/finish/ (PUT)

List Automobile http://localhost:8100/api/automobiles/ (GET)
Show Automobile http://localhost:8100/api/automobiles/:vin/ (GET)
Create Automobile http://localhost:8100/api/automobiles/ (POST)
Delete Automobile http://localhost:8100/api/automobiles/:vin/ (DELETE)
Update Automobile http://localhost:8100/api/automobiles/:vin/ (PUT)

List Manufacturer http://localhost:8100/api/manufacturers/ (GET)
Create Manufacturer http://localhost:8100/api/manufacturers/ (POST)
Show Manufacturer http://localhost:8100/api/manufacturers/:id/ (GET)
Delete Manufacturer http://localhost:8100/api/manufacturers/:id/ (DELETE)
Update Manufacturer http://localhost:8100/api/manufacturers/:id/ (PUT)

List Vehicle http://localhost:8100/api/models/ (GET)
Create Vehicle http://localhost:8100/api/models/ (POST)
Show Vehicle http://localhost:8100/api/models/:id/ (GET)
Delete Vehicle http://localhost:8100/api/models/:id/ (DELETE)
Update Vehicle http://localhost:8100/api/models/:id/ (PUT)

Salesperson
List Salespeople: GET http://localhost:8090/api/salespeople/
Create a Salesperson: POST http://localhost:8090/api/salespeople/

Customers
List Customers: GET http://localhost:8090/api/customers/
Create a Customer: POST http://localhost:8090/api/customers/

Sales
List Sales: GET http://localhost:8090/api/sales/
Create a Sale: POST http://localhost:8090/api/sales/
Sale History : GET http://localhost:8090/api/sales/history/


### Inventory API (Optional)

- Put Inventory API documentation here. This is optional if you have time, otherwise prioritize the other services.

### Service API

The service microservice has 3 models which are: Technician, AutomobileVO, and Appointment. The integration of the two microservices are to make an appointment for service repairs for a vehicle. When automobiles are bought, the vin number is stored and tracked to see if you are a VIP. The service provides a way to make an appointment and choose the technician for that appointment. Through the list of appointments, the appointment status can be updated once the appointment is either caneled or finished which then moves the appointment to the service history.

### Sales API

The Sales Application is designed to manage automobile sales, tracking transactions between salespeople, customers, and the automobiles being sold. It supports listing, creating, and deleting sales. Salespeople can select an autommobile, make a sale, and track all sales made in sale history.

## Value Objects

Automobile = AutomobileVO
The automobile value object has two fields which are vin and sold. Through the two, if a car is sold then the client is known as VIP, if the vehicle sold is false then the client is not VIP. To see whether a vehicle was sold, the vin field is how to specify which vehicle is being called to see if client is VIP.
