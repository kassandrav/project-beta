from django.db import models

class Technician(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    employee_id = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return self.name


class Appointment(models.Model):
    status_choices = [
        ('created', 'Created'),
        ('finished', 'Finished'),
        ('canceled', 'Canceled'),
    ]
    date_time = models.DateTimeField()
    reason = models.CharField(max_length=360)
    status = models.CharField(max_length=10, choices=status_choices, default='created')
    vin = models.CharField(max_length=500, unique=True)
    customer = models.CharField(max_length=100)
    technician = models.ForeignKey(
        Technician,
        related_name="+",
        on_delete=models.PROTECT
    )
    def __str__(self):
        return self.name


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)
