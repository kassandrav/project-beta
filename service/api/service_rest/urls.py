from django.urls import path
from .views import api_list_technicians, api_list_appointments, show_technician, show_appointment, update_status

urlpatterns = {
    path("technicians/", api_list_technicians, name="api_list_technicians"),
    path("technicians/<int:id>/", show_technician, name="show_technician"),
    path("appointments/", api_list_appointments, name="api_list_appointments"),
    path("appointments/<int:id>/", show_appointment, name="show_appointment"),
    path("appointments/<int:id>/", update_status, name="update_status"),
    }
