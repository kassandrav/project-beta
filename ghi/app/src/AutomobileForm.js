import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

function AutomobileForm({ existingAutomobile, onFormSubmit }) {
  const initialState = existingAutomobile || {
    color: '',
    year: '',
    vin: '',
    sold: false,
    model_id: '', 
  };

  const [automobile, setAutomobile] = useState(initialState);
  const [models, setModels] = useState([]);
  const navigate = useNavigate();

  useEffect(() => {
    async function fetchModels() {
      try {
        const response = await fetch('http://localhost:8100/api/models/'); 
        if (response.ok) {
          const { models } = await response.json();
          setModels(models);
        } else {
          throw new Error('Failed to fetch models');
        }
      } catch (error) {
        console.error('Error fetching models:', error);
      }
    }
    fetchModels();
  }, []);

  const handleChange = (event) => {
    const { name, value, type, checked } = event.target;
    setAutomobile(prevState => ({
      ...prevState,
      [name]: type === 'checkbox' ? checked : value,
    }));
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = { ...automobile, model_id: automobile.model_id }; 
    delete data.model;

    const fetchConfig = {
      method: existingAutomobile ? "PUT" : "POST",
      body: JSON.stringify(data),
      headers: { 'Content-Type': 'application/json' },
    };

    const url = existingAutomobile
      ? `http://localhost:8100/api/automobiles/${automobile.vin}/`
      : `http://localhost:8100/api/automobiles/`;

    try {
      const response = await fetch(url, fetchConfig);
      if (response.ok) {
        onFormSubmit && onFormSubmit();
        navigate('/automobiles');
      } else {
        console.error("HTTP error:", response.statusText);
      }
    } catch (error) {
      console.error("Network error:", error);
    }
  };

  
  return (
    <form className="row g-3" onSubmit={handleSubmit}>
      <div className="form-group">
        <label htmlFor="color" className="form-label">Color</label>
        <input type="text" className="form-control" id="color" name="color" value={automobile.color} onChange={handleChange} required />
      </div>
      <div className="form-group">
        <label htmlFor="year" className="form-label">Year</label>
        <input type="number" className="form-control" id="year" name="year" value={automobile.year} onChange={handleChange} required />
      </div>
      <div className="form-group">
        <label htmlFor="vin" className="form-label">VIN</label>
        <input type="text" className="form-control" id="vin" name="vin" value={automobile.vin} onChange={handleChange} required />
      </div>
      <div className="form-group">
        <label htmlFor="model_id" className="form-label">Model</label>
        <select className="form-select" id="model_id" name="model_id" value={automobile.model_id} onChange={handleChange}>
          <option value="">Select a model</option>
          {models.map(model => (
            <option key={model.id} value={model.id}>{model.name}</option>
          ))}
        </select>
      </div>
      <div className="form-check">
        <input className="form-check-input" type="checkbox" id="sold" name="sold" checked={automobile.sold} onChange={handleChange} />
        <label className="form-check-label" htmlFor="sold">Sold</label>
      </div>
      <button type="submit" className="btn btn-primary">Submit</button>
    </form>
  );
}

export default AutomobileForm;
