import React, { useState } from "react";
import { useNavigate } from "react-router-dom";

function CreateTechnician() {
  const [first_name, setFirstName] = useState("");
  const [last_name, setLastName] = useState("");
  const [employee_id, setEmployeeId] = useState("");
  const navigate = useNavigate();

  const handleFirstNameChange = (event) => {
    const value = event.target.value;
    setFirstName(value);
  };

  const handleLastNameChange = (event) => {
    const value = event.target.value;
    setLastName(value);
  };

  const handleEmployeeId = (event) => {
    const value = event.target.value;
    setEmployeeId(value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {
      first_name,
      last_name,
      employee_id,
    };
    console.log(data);

    const technicianUrl = "http://localhost:8080/api/technicians/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(technicianUrl, fetchConfig);
    if (response.ok) {
      const newTechnician = await response.json();
      console.log(newTechnician);

      setFirstName("");
      setLastName("");
      setEmployeeId("");
    }
    navigate("/technicians");
  };
  return (
    <form
      className="row g-3"
      onSubmit={handleSubmit}
      id="create-technician-form"
    >
      <h1 className="d-flex justify-content-center align-items-center">
        Create a Technician
      </h1>
      <div className="d-flex justify-content-center align-items-center">
        <label className="form-control-label px-3" htmlFor="first_name">
          First Name
        </label>
        <input
          className="d-flex justify-content-center align-items-center"
          required
          type="text"
          id="first_name"
          name="first_name"
          placeholder="Enter First Name..."
          value={first_name}
          onChange={handleFirstNameChange}
        />
      </div>
      <div className="d-flex justify-content-center align-items-center">
        <label className="form-control-label px-3" htmlFor="last_name">
          Last Name
        </label>
        <input
          className="d-flex justify-content-center align-items-center"
          required
          type="text"
          id="last_name"
          name="last_name"
          placeholder="Enter Last Name..."
          value={last_name}
          onChange={handleLastNameChange}
        />
      </div>
      <div className="d-flex justify-content-center align-items-center">
        <label className="form-control-label px-3" htmlFor="employee_id">
          Employee ID
        </label>
        <input
          className="d-flex justify-content-center align-items-center"
          required
          type="text"
          id="employee_id"
          name="employee_id"
          placeholder="Enter Employee Id..."
          value={employee_id}
          onChange={handleEmployeeId}
        />
      </div>
      <div className="d-flex justify-content-center align-items-center">
        <button className="btn btn-primary" type="submit">
          Submit
        </button>
      </div>
    </form>
  );
}
export default CreateTechnician;
