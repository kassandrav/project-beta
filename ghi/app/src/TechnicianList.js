import React, { useEffect, useState } from "react";

function TechnicianList() {
  const [technicians, setTechnician] = useState([]);

  const fetchData = async () => {
    const response = await fetch("http://localhost:8080/api/technicians/");
    try {
      if (response.ok) {
        const data = await response.json();
        setTechnician(data.technicians);
      }
    } catch (e) {
      console.log("Error fetching technicians");
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  const handleDelete = async (technicianId) => {
    const response = await fetch(`http://localhost:8080/api/technicians/${technicianId}`, {
      method: 'DELETE'
    })
    try {
      if (response.ok) {
        fetchData();
      }
    } catch (e) {
      console.log('Error Deleting item', e)
    }
  };

  return (
    <div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Employee ID</th>
            <th>Delete</th>
          </tr>
        </thead>
        <tbody>
          {technicians.map((technician) => {
            return (
              <tr key={technician.id}>
                <td>{ technician.first_name }</td>
                <td>{ technician.last_name }</td>
                <td>{ technician.employee_id }</td>
                <td>
                  <button className="btn btn-danger" onClick={() => handleDelete(technician.id)}>Delete</button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

export default TechnicianList;
