import React, { useState, useEffect } from 'react';

function SalesHistory() {
    const [salespeople, setSalespeople] = useState([]);
    const [selectedSalesperson, setSelectedSalesperson] = useState('');
    const [sales, setSales] = useState([]);

    useEffect(() => {
        fetch('http://localhost:8090/api/salespeople/')
            .then((response) => response.json())
            .then((data) => setSalespeople(data.salespeople));
    }, []);

    useEffect(() => {
        if (selectedSalesperson) {
            fetch(`http://localhost:8090/api/sales/salesperson/${selectedSalesperson}`)
                .then((response) => response.json())
                .then((data) => setSales(data.sales));
        }
    }, [selectedSalesperson]);

    const handleSalespersonChange = (event) => {
        setSelectedSalesperson(event.target.value);
    };

    return (
        <div>
            <h2>Sales History by Sales Person</h2>
            <div className="mb-3">
                <label htmlFor="salesperson" className="form-label">Select Sales Person:</label>
                <select id="salesperson" onChange={handleSalespersonChange} className="form-control">
                    <option value="">Select a Sales Person</option>
                    {salespeople.map(({ id, first_name, last_name, employee_id }) => (
                        <option key={id} value={id}>{`${first_name} ${last_name} - ${employee_id}`}</option>
                    ))}
                </select>
            </div>
            {selectedSalesperson && (
                <table className="table">
                    <thead>
                        <tr>
                            <th>Sales Person Name</th>
                            <th>Customer Name</th>
                            <th>Automobile VIN</th>
                            <th>Sale Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        {sales.map(({ id, salesperson, customer, automobile, price }) => ( // Updated to include salesperson in map function
                            <tr key={id}>
                                <td>{`${salesperson.first_name} ${salesperson.last_name}`}</td>
                                <td>{`${customer.first_name} ${customer.last_name}`}</td>
                                <td>{automobile.vin}</td>
                                <td>${price}</td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            )}
        </div>
    );
}

export default SalesHistory;