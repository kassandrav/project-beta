import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";

function CreateAppointment() {
  const [vin, setVin] = useState("");
  const [customer, setCustomer] = useState("");
  const [date_time, setDateTime] = useState("");
  const [technician, setTechnician] = useState("");
  const [technicians, setTechnicians] = useState([]);
  const [reason, setReason] = useState("");
  const navigate = useNavigate();

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetch("http://localhost:8080/api/technicians");
        if (response.ok) {
          const data = await response.json();
          setTechnicians(data.technicians);
        }
      } catch (e) {
        console.log("Error fetching locations:", e);
      }
    };

    fetchData();
  }, []);

  const handleVinChange = (event) => {
    const value = event.target.value;
    setVin(value);
  };

  const handleCustomerChange = (event) => {
    const value = event.target.value;
    setCustomer(value);
  };

  const handleDateTimeChange = (event) => {
    const value = event.target.value;
    setDateTime(value);
  };

  const handleTechnicianChange = (event) => {
    const value = event.target.value;
    setTechnician(value);
  };

  const handleReasonChange = (event) => {
    const value = event.target.value;
    setReason(value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {
      vin,
      customer,
      date_time,
      technician,
      reason,
    };
    console.log(data);

    const appointmentnUrl = "http://localhost:8080/api/appointments/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(appointmentnUrl, fetchConfig);
    if (response.ok) {
      const newAppointment = await response.json();
      console.log(newAppointment);

      setVin("");
      setCustomer("");
      setDateTime("");
      setTechnician("");
      setReason("");
    }
    navigate("/appointments");
  };
  return (
    <form
      className="row g-3"
      onSubmit={handleSubmit}
      id="create-appointment-form"
    >
      <h1 className="d-flex justify-content-center align-items-center">
        Make An Appointment
      </h1>
      <div className="form-group">
        <label className="form-control-label px-3" htmlFor="vin">
          Autobmobile VIN
        </label>
        <input
          className="form-control"
          type="text"
          id="vin"
          name="vin"
          placeholder="Enter VIN..."
          value={vin}
          onChange={handleVinChange}
        />
      </div>
      <div className="form-group">
        <label className="form-control-label px-3" htmlFor="customer">
          Customer
        </label>
        <input
          className="form-control"
          type="text"
          id="customer"
          name="customer"
          placeholder="Enter Name..."
          value={customer}
          onChange={handleCustomerChange}
        />
      </div>
      <div className="form-group">
        <label className="form-control-label px-3" htmlFor="date_time">
          Date
        </label>
        <input
          className="form-control"
          type="datetime-local"
          id="date_time"
          name="date_time"
          placeholder="date_time"
          value={date_time}
          onChange={handleDateTimeChange}
        />
      </div>
      <div className="form-group">
        <label className="form-control-label px-3" htmlFor="reason">
          Reason
        </label>
        <input
          className="form-control"
          type="text"
          id="reason"
          name="reason"
          placeholder="Enter Reason For Appointment..."
          value={reason}
          onChange={handleReasonChange}
        />
      </div>
      <label className="form-control-label px-3" htmlFor="technician">
        Technician
      </label>
      <select
        value={technician}
        onChange={handleTechnicianChange}
        required
        name="technician"
        id="technician"
        className="form-select"
      >
        <option>Choose a Technician</option>
        {technicians.map((technician) => {
          return (
            <option key={technician.id} value={technician.id}>
              {technician.first_name} {technician.last_name}
            </option>
          );
        })}
        ;
      </select>
      <div className="d-flex justify-content-center align-items-center">
        <button className="btn btn-primary" type="submit">
          Submit
        </button>
      </div>
    </form>
  );
}
export default CreateAppointment;
