import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

function CreateSale() {
    const [automobiles, setAutomobiles] = useState([]);
    const [customers, setCustomers] = useState([]);
    const [salespeople, setSalespeople] = useState([]);
    const navigate = useNavigate();

    const initialState = {
        automobile: '',
        customer: '',
        salesperson: '',
        price: '',
    };
    const [sale, setSale] = useState(initialState);

    useEffect(() => {
        async function fetchData() {
            try {
                const autoResponse = await fetch('http://localhost:8100/api/automobiles/');
                const customerResponse = await fetch('http://localhost:8090/api/customers/');
                const salespersonResponse = await fetch('http://localhost:8090/api/salespeople/');
                const autoData = await autoResponse.json();
                const customerData = await customerResponse.json();
                const salespersonData = await salespersonResponse.json();

                setAutomobiles(autoData.autos);
                setCustomers(customerData.customers);
                setSalespeople(salespersonData.salespeople);
            } catch (error) {
                console.error('Error fetching data:', error);
            }
        }
        fetchData();
    }, []);

    const handleChange = (event) => {
        const { name, value } = event.target;
        setSale({ ...sale, [name]: value });
    };

    const handleSubmit = async (event) => {
        event.preventDefault();
        const salePayload = { ...sale, price: parseFloat(sale.price) };
        try {
            const response = await fetch('http://localhost:8090/api/sales/', {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(salePayload),
            });
            if (response.ok) {
                alert('Sale created successfully');
                navigate('/sales');
            } else {
                throw new Error(`Failed to create sale: ${response.statusText}`);
            }
        } catch (error) {
            console.error("Error adding sale:", error);
            alert("Error adding sale: " + error.message);
        }
    };

    return (
        <div className="container my-4">
            <h2>Create Sale</h2>
            <form onSubmit={handleSubmit}>
                <div className="mb-3">
                    <label htmlFor="automobile" className="form-label">Select Automobile:</label>
                    <select id="automobile" name="automobile" value={sale.automobile} onChange={handleChange} required className="form-control">
                        <option value="">Select an Automobile</option>
                        {automobiles.map(auto => (
                            <option key={auto.id} value={auto.id}>{auto.vin}</option>
                        ))}
                    </select>
                </div>
                <div className="mb-3">
                    <label htmlFor="customer" className="form-label">Select Customer:</label>
                    <select id="customer" name="customer" value={sale.customer} onChange={handleChange} required className="form-control">
                        <option value="">Select a Customer</option>
                        {customers.map(customer => (
                            <option key={customer.id} value={customer.id}>{customer.first_name} {customer.last_name}</option>
                        ))}
                    </select>
                </div>
                <div className="mb-3">
                    <label htmlFor="salesperson" className="form-label">Select Salesperson:</label>
                    <select id="salesperson" name="salesperson" value={sale.salesperson} onChange={handleChange} required className="form-control">
                        <option value="">Select a Salesperson</option>
                        {salespeople.map(salesperson => (
                            <option key={salesperson.id} value={salesperson.id}>{salesperson.first_name} {salesperson.last_name}</option>
                        ))}
                    </select>
                </div>
                <div className="mb-3">
                    <label htmlFor="price" className="form-label">Sale Price:</label>
                    <input type="number" id="price" name="price" value={sale.price} onChange={handleChange} required className="form-control" />
                </div>
                <button type="submit" className="btn btn-primary">Create Sale</button>
            </form>
        </div>
    );
}

export default CreateSale;