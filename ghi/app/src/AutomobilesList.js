import React, { useState, useEffect } from 'react';

function AutomobileList() {
  const [automobiles, setAutomobiles] = useState([]);


  const fetchData = async () => {
    try {
      const response = await fetch('http://localhost:8100/api/automobiles/');
      if (response.ok) {
        const data = await response.json();
        setAutomobiles(data.autos);
      } else {
        console.error("Failed to fetch automobiles: ", response.statusText);
      }
    } catch (e) {
      console.error("Error fetching Automobiles: ", e);
    }
  };


  useEffect(() => {
    fetchData();
  }, []);


  const handleDelete = async (automobileId) => {
    try {
      const response = await fetch(`http://localhost:8100/api/automobiles/${automobileId}`, {
        method: 'DELETE',
      });
      if (response.ok) {
        fetchData();
      } else {
        console.error("Failed to delete automobile: ", response.statusText);
      }
    } catch (e) {
      console.error("Error deleting automobile: ", e);
    }
  };


  return (
    <div>
      <h2>Automobiles Inventory</h2>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Color</th>
            <th>Year</th>
            <th>VIN</th>
            <th>Model</th>
            <th>Sold</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {automobiles.map(auto => (
            <tr key={auto.vin}>
              <td>{auto.color}</td>
              <td>{auto.year}</td>
              <td>{auto.vin}</td>
              <td>{auto.model.name}</td> 
              <td>{auto.sold ? 'Yes' : 'No'}</td>
              <td>
                <button className="btn btn-danger" onClick={() => handleDelete(auto.vin)}>Delete</button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default AutomobileList;