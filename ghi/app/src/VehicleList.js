import React, { useEffect, useState } from "react";

function VehicleModel() {
  const [models, setVehicleModel] = useState([]);

  const fetchData = async () => {
    const response = await fetch("http://localhost:8100/api/models/");
    try {
      if (response.ok) {
        const data = await response.json();
        setVehicleModel(data.models);
      }
    } catch (e) {
      console.log("Error fetching vehicle model data");
    }
  };
  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Name</th>
            <th>Manufacturer</th>
            <th>Picture</th>
          </tr>
        </thead>
        <tbody>
          {models.map((model) => {
            return (
              <tr key={model.id}>
                <td>{model.name}</td>
                <td>{model.manufacturer.name}</td>
                <td>
                  <img
                    src={model.picture_url}
                    alt={model.picture_url}
                    style={{ maxWidth: "80px" }}
                  />
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}
export default VehicleModel;
