import React, { useState } from "react";
import { useNavigate } from "react-router-dom";

function CreateManufacturer() {
  const [name, setName] = useState("");
  const navigate = useNavigate();

  const handleNameChange = (event) => {
    const value = event.target.value;
    setName(value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {
      name,
    };
    console.log(data);

    const manufacturerUrl = "http://localhost:8100/api/manufacturers/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(manufacturerUrl, fetchConfig);
    if (response.ok) {
      const newManufacturer = await response.json();
      console.log(newManufacturer);

      setName("");
    }
    navigate("/manufacturers");
  };
  return (
    <form
      className="row g-3"
      onSubmit={handleSubmit}
      id="create-manufacturer-form"
    >
      <div className="row g-2">
        <h1>Create a Manufacturer</h1>
        <label className="form-control-label px-3" htmlFor="name">
          Manufacturer Name
        </label>
        <input
          className="form-control"
          required
          type="text"
          id="name"
          name="name"
          placeholder="Enter Manufacturer..."
          value={name}
          onChange={handleNameChange}
        />
      </div>
      <div className="d-flex justify-content-center align-items-center">
        <button className="btn btn-primary" type="submit">
          Submit
        </button>
      </div>
    </form>
  );
}
export default CreateManufacturer;
