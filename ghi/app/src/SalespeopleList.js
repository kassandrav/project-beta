import React, { useEffect, useState } from 'react';

function SalespeopleList() {
  const [salespeople, setSalespeople] = useState([]);

  useEffect(() => {
    async function fetchSalespeople() {
      try {
        const response = await fetch('http://localhost:8090/api/salespeople/');
        if (!response.ok) throw new Error('Failed to fetch salespeople');
        const data = await response.json();
        setSalespeople(data.salespeople); 
      } catch (error) {
        console.error('Error fetching salespeople:', error);
      }
    }

    fetchSalespeople();
  }, []);

  return (
    <div>
      <h2>Salespeople List</h2>
      <table className="table">
        <thead>
          <tr>
            <th>Employee ID</th>
            <th>First Name</th>
            <th>Last Name</th>
          </tr>
        </thead>
        <tbody>
          {salespeople.map(({ id, first_name, last_name, employee_id }) => (
            <tr key={id}>
              <td>{employee_id}</td>
              <td>{first_name}</td>
              <td>{last_name}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default SalespeopleList;