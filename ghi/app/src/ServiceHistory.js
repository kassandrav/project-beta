import React, { useEffect, useState } from "react";

function ServiceHistory() {
  const [appointments, setAppointments] = useState([]);
  const [value, setValue] = useState("");

  const onChange = (event) => {
    setValue(event.target.value);
  };

  const onSearch = (searchTerm) => {
    console.log("search", searchTerm);
  };

  const fetchData = async () => {
    const response = await fetch("http://localhost:8080/api/appointments/");
    try {
      if (response.ok) {
        const data = await response.json();
        setAppointments(data.appointments);
      }
    } catch (e) {
      console.log("Error fetching appointments");
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div>
      <h1>Service History</h1>
      <div className="search-container">
        <div className="search-inner">
          <input type="text" placeholder="Enter VIN..." value={value} onChange={onChange}></input>
          <button type='submit' onClick={() => onSearch(value)}>Search</button>
        </div>
      </div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>VIN</th>
            <th>Customer</th>
            <th>Date/Time</th>
            <th>Technician</th>
            <th>Reason</th>
            <th>Status</th>
          </tr>
        </thead>
        <tbody>
          {appointments.filter((appointment) => {
            return value.toLowerCase() === '' ? appointment : appointment.vin.toLowerCase().includes(value)
          }).map((appointment) => {
            return (
              <tr key={appointment.id}>
                <td>{appointment.vin}</td>
                <td>{appointment.customer}</td>
                <td>{appointment.date_time}</td>
                <td>{appointment.technician.first_name}</td>
                <td>{appointment.reason}</td>
                <td>{appointment.status}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

export default ServiceHistory;
