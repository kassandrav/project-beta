import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';

function CreateSalesperson() {
  const [salesperson, setSalesperson] = useState({
    first_name: '',
    last_name: '',
    employee_id: '',
  });
  const navigate = useNavigate();

  const handleChange = (event) => {
    const { name, value } = event.target;
    setSalesperson((prevState) => ({ ...prevState, [name]: value }));
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    try {
      const response = await fetch('http://localhost:8090/api/salespeople/', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(salesperson),
      });
      if (response.ok) {
        navigate('/salespeople');
      } else {
        const errorText = await response.text();
        throw new Error('Failed to create salesperson: ' + errorText);
      }
    } catch (error) {
      console.error("Error adding salesperson:", error.message);
      alert("Error adding salesperson: " + error.message);
    }
  };

  return (
    <div className="container my-4">
      <h2>Create Salesperson</h2>
      <form onSubmit={handleSubmit}>
        <div className="mb-3">
          <label htmlFor="first_name" className="form-label">First Name:</label>
          <input type="text" id="first_name" name="first_name" className="form-control" value={salesperson.first_name} onChange={handleChange} required />
        </div>
        <div className="mb-3">
          <label htmlFor="last_name" className="form-label">Last Name:</label>
          <input type="text" id="last_name" name="last_name" className="form-control" value={salesperson.last_name} onChange={handleChange} required />
        </div>
        <div className="mb-3">
          <label htmlFor="employee_id" className="form-label">Employee ID:</label>
          <input type="text" id="employee_id" name="employee_id" className="form-control" value={salesperson.employee_id} onChange={handleChange} required />
        </div>
        <button type="submit" className="btn btn-primary">Create Salesperson</button>
      </form>
    </div>
  );
}

export default CreateSalesperson;